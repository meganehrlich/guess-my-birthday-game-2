# Random to generate random numbers for month and year
import random

# Aesthetic choice so lines don't all appear at once
from time import sleep

# Variable for user input name
user_name = input("What is your name? > ")

#Say hello user
print(f"Hello {user_name}!  I'm going to guess your birthday!")

# Game has 5 attempts before terminating
counter = 5

while counter > 0:
    # Variables for random month, year, and randomly generated computer guess
    rand_month = random.randint(1, 12)
    rand_year = random.randint(1900, 2022)
    computer_guess = (f"Is your birthday {rand_month} {rand_year}?")

    # Display randomly generated guess and prompt user yes or no
    print(computer_guess)
    user_answer = input("Type yes or no > ")
 
    # If user says guess is correct display yay and end game    
    if user_answer == "yes":
        counter = 0
        print("I knew it!")
        sleep(0.5)
        print("Thanks for playing!")
        exit

    # If user says guess is wrong subtract 1 from counter and try again
    # If counter reaches 0 tell player and end game    
    elif user_answer == "no":
        counter -= 1
        if counter == 0:
            print(f"Oh well {user_name} maybe next time")
        else:
            print("Drat! Let me try again!")
        
    # Accounting for human error, if any other answer display error message    
    # Sleep for aesthetic purposes
    else:
        print("Error! Not a valid answer, I'll try again.")
        sleep(0.5)
